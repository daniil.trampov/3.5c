// Copyright Epic Games, Inc. All Rights Reserved.

#include "MyProject8GameMode.h"
#include "MyProject8HUD.h"
#include "MyProject8Character.h"
#include "UObject/ConstructorHelpers.h"

AMyProject8GameMode::AMyProject8GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AMyProject8HUD::StaticClass();
}
